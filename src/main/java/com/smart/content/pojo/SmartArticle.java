package com.smart.content.pojo;

import lombok.Data;

/**
 * 通用的文章对象
 *
 * @author chentiefeng
 * @date 2021/2/9 15:07
 */
@Data
public class SmartArticle {
    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

}
