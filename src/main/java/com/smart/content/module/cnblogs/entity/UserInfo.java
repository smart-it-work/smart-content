package com.smart.content.module.cnblogs.entity;

import lombok.Data;

/**
 * cnblogs 用户信息
 *
 * @author chentiefeng
 * @date 2021/2/9 13:49
 */
@Data
public class UserInfo {
    /**
     * 示例：imchentiefeng@aliyun.com
     */
    private String loginName;
    /**
     * 示例：陈铁锋
     */
    private String displayName;
    /**
     * 示例：imchentiefeng@aliyun.com
     */
    private String email;
    /**
     * 示例：imchentiefeng
     */
    private String alias;
    /**
     * 示例：//pic.cnblogs.com/face/1730815/20210204145424.png
     */
    private String iconName;
    /**
     * 示例：//pic.cnblogs.com/avatar/1730815/20210204145424.png
     */
    private String avatarName;
    /**
     * 示例：1730815
     */
    private String spaceUserId;
    /**
     * 示例：imchentiefeng
     */
    private String blogApp;
    /**
     * 示例：663756
     */
    private String blogId;
}
