package com.smart.content.module.cnblogs.entity;

import lombok.Data;

/**
 * 标签信息
 *
 * @author chentiefeng
 * @date 2021/2/9 14:17
 */
@Data
public class TagInfo {
    /**
     * 示例：4147838
     */
    private Integer id;
    /**
     * 示例：springboot
     */
    private String name;
    /**
     * 示例：3
     */
    private Integer useCount;
    /**
     * 示例：3
     */
    private Integer privateUseCount;
    /**
     * 示例："2021-02-07T22:47:45.57"
     */
    private String createTime;
}
