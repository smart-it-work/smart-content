package com.smart.content.module.cnblogs.entity;

import lombok.Data;

import java.util.List;

/**
 * 文章列表展示信息
 *
 * @author chentiefeng
 * @date 2021/2/9 14:06
 */
@Data
public class ArticleListInfo {
   private Integer pageSize;
   private Integer postsCount;
   private String categoryName;
   private List<ArticleInfo> postList;
}
