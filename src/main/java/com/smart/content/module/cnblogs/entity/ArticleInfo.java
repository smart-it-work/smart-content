package com.smart.content.module.cnblogs.entity;

/**
 * 文章信息
 *
 * @author chentiefeng
 * @date 2021/2/9 14:06
 */
public class ArticleInfo {
    /**
     * 示例：14391391
     */
    private Integer id;
    /**
     * 示例：SpringBoot——容器工具类SpringContextUtils.java
     */
    private String title;
    /**
     * 示例：//www.cnblogs.com/imchentiefeng/articles/springboot-context-utils.html
     */
    private String url;
    /**
     * 示例：true
     */
    private boolean isPublished;
    /**
     * 示例：0
     */
    private Integer feedBackCount;
    /**
     * 示例：0
     */
    private Integer webCount;
    /**
     * 示例：0
     */
    private Integer aggCount;
    /**
     * 示例：0
     */
    private Integer viewCount;
    /**
     * 示例：2021-02-08T22:52:00
     */
    private String datePublished;
    /**
     * 示例：springboot-context-utils
     */
    private String entryName;
    /**
     * 示例：2
     */
    private int postType;
    /**
     * 示例：16477
     */
    private int postConfig;
    /**
     * 示例：2021-02-08T22:54:00
     */
    private String dateUpdated;
}
