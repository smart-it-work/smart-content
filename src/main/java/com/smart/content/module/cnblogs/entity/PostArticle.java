package com.smart.content.module.cnblogs.entity;

import lombok.Data;

import java.util.List;

/**
 * 保存的文章信息
 *
 * @author chentiefeng
 * @date 2021/2/9 15:03
 */
@Data
public class PostArticle {

    /**
     * 示例：null
     */
    private String id;
    /**
     * 示例：
     */
    private int postType;
    /**
     * 示例：
     */
    private int accessPermission;
    /**
     * 示例：
     */
    private String title;
    /**
     * 示例：
     */
    private String url;
    /**
     * 示例：
     */
    private String postBody;
    /**
     * 示例：
     */
    private List<Integer> categoryIds ;
    /**
     * 示例：
     */
    private boolean inSiteCandidate;
    /**
     * 示例：
     */
    private boolean inSiteHome;
    /**
     * 示例：
     */
    private String siteCategoryId;
    /**
     * 示例：
     */
    private String blogTeamIds;
    /**
     * 示例：
     */
    private boolean isPublished;
    /**
     * 示例：
     */
    private boolean displayOnHomePage;
    /**
     * 示例：
     */
    private boolean isAllowComments;
    /**
     * 示例：
     */
    private boolean includeInMainSyndication;
    /**
     * 示例：
     */
    private boolean isPinned;
    /**
     * 示例：
     */
    private boolean isOnlyForRegisterUser;
    /**
     * 示例：
     */
    private boolean isUpdateDateAdded;
    /**
     * 示例：
     */
    private String entryName;
    /**
     * 示例：
     */
    private String description;
    /**
     * 示例：
     */
    private List<String> tags ;
    /**
     * 示例：
     */
    private String password;
    /**
     * 示例：
     */
    private String datePublished;
    /**
     * 示例：
     */
    private boolean isMarkdown;
    /**
     * 示例：
     */
    private boolean isDraft;
    /**
     * 示例：
     */
    private String autoDesc;
    /**
     * 示例：
     */
    private boolean changePostType;
    /**
     * 示例：
     */
    private int blogId;
    /**
     * 示例：
     */
    private String author;
    /**
     * 示例：
     */
    private boolean removeScript;
    /**
     * 示例：
     */
    private String clientInfo;
    /**
     * 示例：
     */
    private boolean changeCreatedTime;
    /**
     * 示例：
     */
    private boolean canChangeCreatedTime;
}
