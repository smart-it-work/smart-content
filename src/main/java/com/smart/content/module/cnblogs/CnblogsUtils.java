package com.smart.content.module.cnblogs;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.smart.common.util.JsonUtils;
import com.smart.content.module.cnblogs.entity.ArticleListInfo;
import com.smart.content.module.cnblogs.entity.PostArticle;
import com.smart.content.module.cnblogs.entity.TagInfo;
import com.smart.content.module.cnblogs.entity.UserInfo;
import com.smart.content.pojo.SmartArticle;

import javax.naming.AuthenticationException;
import java.util.List;

/**
 * cnblogs登陆工具类
 *
 * @author chentiefeng
 * @date 2021/2/9 13:45
 */
public class CnblogsUtils {
    public static void main(String[] args) throws AuthenticationException {
        String cookieValue = getCookieValue();
        List<TagInfo> userInfo = getTags(cookieValue);

        String markdownContent = "# test\n" +
                "## test2\n" +
                "### test3";

        System.out.println(markdownTranslate(cookieValue, markdownContent));
    }
//    public static String addContent(String cookieValue, SmartArticle smartArticle) {
//        PostArticle postArticle = new PostArticle();
//        postArticle.setTitle(smartArticle.getTitle());
//        postArticle.setPostBody(smartArticle.getContent());
//
//
//    }
    /**
     * 翻译MarkDown内容为html内容
     * @param cookieValue
     * @param markdownContent
     * @return
     * @throws AuthenticationException
     */
    public static String markdownTranslate(String cookieValue, String markdownContent) throws AuthenticationException {
        String url = "https://i.cnblogs.com/api/markdown/translate";
        HttpResponse response = HttpRequest.post(url)
                .header("cookie", cookieValue)
                .body(markdownContent)
                .execute();
        if (response.getStatus() == 401) {
            throw new AuthenticationException("身份认证失败");
        }
        return response.body();
    }
    /**
     * 获取Tag标签集合
     * @param cookieValue
     * @return
     * @throws AuthenticationException
     */
    public static List<TagInfo> getTags(String cookieValue) throws AuthenticationException {
        String articleListUrl = "https://i.cnblogs.com/api/tags/list";
        return JsonUtils.parseToList(getResponseText(articleListUrl, cookieValue), TagInfo.class);
    }
    /**
     * 获取文章列表信息
     * @param cookieValue
     * @return
     * @throws AuthenticationException
     */
    public static ArticleListInfo getArticleListInfo(String cookieValue) throws AuthenticationException {
        String articleListUrl = "https://i.cnblogs.com/api/posts/list";
        return JsonUtils.parse(getResponseText(articleListUrl, cookieValue), ArticleListInfo.class);
    }
    /**
     * 获取用户信息
     * @param cookieValue
     * @return
     * @throws AuthenticationException
     */
    public static UserInfo getUserInfo(String cookieValue) throws AuthenticationException {
        String userInfoUrl = "https://i.cnblogs.com/api/user";
        return JsonUtils.parse(getResponseText(userInfoUrl, cookieValue), UserInfo.class);
    }

    /**
     * 获取Cookie的值
     * @return
     */
    public static String getCookieValue() {
        return ".Cnblogs.AspNetCore.Cookies=CfDJ8EklyHYHyB5Oj4onWtxTnxYR6OlCDXSOkZJ9EsBQUtXvlUnjhr4QAJAMzVe_0w2doAswTPOYI4Vk_qeo4MbaQZCEYft3WLsM1hXvo-WRL1N_6InASPZ4UGog4MxvKECvBD8mVtpZ14FGhCMRtmXXmgf0K_AA47F_LsqCQh6_94Lg1Dr0viQ1rK7ujgmanYyPAyKLOqd0gd2HFGYJM6BMRKKRO2DnWOsJbEOZ_S5tyqCgm6D3m70dQd85q2vxQixzA9842TXcjW4jLe-aOIWfoYZa1w0FJnosXdf1ENi6uIiGQMh26enKzoiwO6QO2DEfPRk3QSxO12ZZdWV2Xm0tHrBO_Gwf_qf1HIwOPhIO4T6WVow9VcQwJeGPZT2ovfNIK0KU418dLtgTpAIxaTsNa2sb6aMUhyhXd3LieBAMDRQTvJZOOfptork87XBBDypuz7wJ45UbKemQlOCJ566YFK0ExnG_fWlrCP9S6MX6BVYxIYaj4mP66YSlAv9PgLcCA8CiH9znBP2fzgxMJCLCf-TjeceTaOfJ7b6ARuUZ6PR1YWEq7j_a-VG_nb0B4YbeZuGQ9sLSJA7WqhBeV5-eOss; .CNBlogsCookie=730558A307F7CC4FBE2324C74A897356285EA892ACD190AF60186DEEA6911CD6A7AE9DE6FE4CA00D4F52C83380AC202EAEB9CA27FBDA372F7EC1AB8DD3A2E7F7B9BABF9BBED01DBBF2058ED1AE392B80D68A3C442006026A7542A24051FDCDAF1BC1238748C4C87A9DF0E8FB9FD792456A0BE205";
    }

    /**
     * 执行请求
     * @param url
     * @param cookieValue
     * @return
     * @throws AuthenticationException
     */
    private static String getResponseText(String url, String cookieValue) throws AuthenticationException {
        HttpResponse response = HttpRequest.get(url)
                .header("cookie", cookieValue)
                .execute();
        if (response.getStatus() == 401) {
            throw new AuthenticationException("身份认证失败");
        }
        return response.body();
    }
}
