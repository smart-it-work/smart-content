package com.smart.content.module.cnblogs.entity;

/**
 * 分类信息
 *
 * @author chentiefeng
 * @date 2021/2/9 15:13
 */
public class CategoryInfo {
    /**
     * 示例：1928144
     */
    private Integer categoryId;
    /**
     * 示例：java并发
     */
    private String title;
    /**
     * 示例：java并发编程
     */
    private String description;
    /**
     * 示例：2021-02-04T15:11:52.407
     */
    private String updateTime;
    /**
     * 示例：true
     */
    private boolean visible;
}
